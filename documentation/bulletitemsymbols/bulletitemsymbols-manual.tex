%%
%
% bulletitemsymbols is a LaTeX package providing small, colorful
% symbols that can be used instead of plain itemize lists' bullets.
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status `maintained'.
%
% The Current Maintainer of this work is
%    Thomas Fischer <fischer@unix-ag.uni-kl.de>
%
% This work consists of the files bulletitemsymbols.sty and
% bulletitemsymbols-manual.tex.
%
%%

\documentclass[a4paper,10pt]{article}
\usepackage[english]{babel}
\usepackage{graphicx}
\usepackage[pdfborder={0 0 0}]{hyperref}
\usepackage{booktabs}
\usepackage{listings}
\usepackage{relsize}
\usepackage{MnSymbol}
\usepackage[svgnames,dvipsnames]{xcolor}
\usepackage{array}
\usepackage{bulletitemsymbols}

\lstset{%
language=[LaTeX]TeX,% all shown code is LaTeX code
basicstyle=\ttfamily,% default font formatting
keywordstyle=\color{green!50!black}\textbf,% how to format recognized keywords
breaklines=true,% allow to break long lines
breakindent=5pt,% indent wrapped/broken lines
prebreak={\mbox{\ensuremath{\rhookswarrow}}},% show a nice symbol when breaking lines
showtabs=false,% show tabulators?
tabsize=4,% step widths of tabulators
numbers=left,% show line numbers on the left
numberstyle=\relsize{-2},% small font size for numbers
numberblanklines=false,% no line numbers for empty lines
numbersep=2ex,% horizontal spacing between numbers and code
xleftmargin=2em,% ???
floatplacement=t,% ask LaTeX to place float on the top of a page
morekeywords={biscolor,bisshape,itembulletEmpty,itembulletMinus,itembulletArrowRight,itembulletArrowLeft,itembulletArrowDown,itembulletArrowUp,itembulletArrowNorthWest,itembulletArrowNorthEast,itembulletArrowSouthWest,itembulletArrowSouthEast,itembulletX,itembulletCheckmark,itembulletExclamationMarkRed,itembulletExclamationMarkYellow,itembulletInformation,itembulletQuestionMark,itembulletPlus,itembulletYellowBullseye,itembulletBlueBullseye,useritembullet}% include commands provided by bulletitemsymbols
}

\newcommand{\hreffootnote}[2]{\href{#1}{#2}\footnote{\href{#1}{\texttt{#1}}}}
\newcommand{\dc}[1]{\texttt{#1}}
\newcommand{\bulletitemsymbols}{\dc{bulletitemsymbols}}
\newcommand{\TikZ}{Ti\textit{k}Z}

\title{\bulletitemsymbols{} -- A \LaTeX{} Package Providing Small, Colorful Symbols\footnote{This manual is written for \bulletitemsymbols{} designated as `2014/04/14 Bullet Item Symbols'. Commands and options may differ in older or newer versions.}}
\author{Thomas Fischer\\\url{fischer@unix-ag.uni-kl.de}\\\url{https://gitlab.com/latex-styles-and-classes/latex-styles-and-classes}}
\date{April 14, 2014}


\begin{document}

\maketitle

\begin{abstract}
\bulletitemsymbols{} is a \LaTeX{} package providing small, colorful symbols that can be used instead of plain itemize lists' bullets.
\end{abstract}

\section{Introduction}

Bullet item lists are a common but often despised element of slide presentations.
To make a item list-heavy presentation more varied and easier to grasp, standard bullets in \dc{itemize} lists can be replaced by arbitrary text or images.

This package provides the means to create small, colorful images suitable as item bullet replacements.
Those images provide quick visual feedback to the viewer about the item's content; their design is supposed to be intuitive and universal.

\begin{itemize}
\item[\itembulletPlus] Quick and easy to grasp symbols
\item[\itembulletPlus] 100\,\% \TikZ
\item[\itembulletMinus] May increase compilation time slightly
\end{itemize}

\section{Realization}

The symbols are drawn using \hreffootnote{http://ctan.org/pkg/pgf}{\TikZ} and have approximately the height of an upper case letter.
The symbols are meant to be easily recognizable even for visually impaired viewers under bad lighting conditions.

{\TikZ} provides great flexibility in the realization of symbols.
A classical font's symbols may only be set in one color, whereas this package's symbols can have multiple colors or gradients.
Parameters and package options allow to adjust symbols to the user's needs, such as switching between circular or square symbols or creating black/white symbols for handouts.

\section{Predefined Symbols}

Users can pick from predefined symbols (explained in this section) or define their own symbols (see Section~\ref{sec:userdefinedsymbols}).

\subsection{Configuration}
\label{sec:predefinedsymbolsconfiguration}

Two aspects of predefined symbols can be configured:

\begin{description}
\item[Color] Symbols can be set in their respective color (default) or set to be black and white only.
Enforcing black/white output is useful if the same \LaTeX\ code is to be used both for colorful slides and black/white handouts.

As package options, color or black/white mode can be configured as default for the whole document.
See Section~\ref{sec:packageoptions} for a detailed description.

Within a document, switching between both modes is done through the commands \lstinline|\biscolor{color}| (enable color mode) and \lstinline|\biscolor{blackwhite}| (enable black/white mode).

\itembulletInformation\ Both the command \lstinline|\biscolor| and the package options \lstinline|color| and \lstinline|blackwhite| affect users-defined symbols as well.

\item[Shape] Symbols can be set either as circles (default) or squares.
Again, there are both package options (see Section~\ref{sec:packageoptions}) and in-document commands to switch between both shapes.

Within a document, switching between both shapes in done through the commands \lstinline|\bisshape{circle}| (circle as base shape) and \lstinline|\bisshape{square}| (square shape).
\end{description}

\subsection{Commands}

The available predefined symbols are shown in Table~\ref{tab:predefinedsymbols}.
An example on how to use the predefined symbols is shown below:

\begin{lstlisting}
% In your preamble, load the package:
\usepackage{bulletitemsymbols}
\end{lstlisting}

\begin{lstlisting}[firstnumber=42]
% Later, in your document:
\begin{itemize}
\item Normal item with standard bullet
      (depending on your \LaTeX{} class or Beamer style)
\item[\itembulletPlus] Runs fast and smooth
\item[\itembulletMinus] Requires loading another package
\end{itemize}

For handouts, you may want to switch to black/white symbols.
\biscolor{blackwhite}
\begin{itemize}
\item Normal item with standard bullet
      (depending on your \LaTeX{} class or Beamer style)
\item[\itembulletPlus] Runs fast and smooth
\item[\itembulletMinus] Requires loading another package
\end{itemize}
\end{lstlisting}

Will result in a text like this:

\begin{itemize}
\item Normal item with standard bullet (depending on your \LaTeX{} class or Beamer style)
\item[\itembulletPlus] Runs fast and smooth
\item[\itembulletMinus] Requires loading another package
\end{itemize}

For handouts, you may want to switch to black/white symbols.
\biscolor{blackwhite}
\begin{itemize}
\item Normal item with standard bullet (depending on your \LaTeX{} class or Beamer style)
\item[\itembulletPlus] Runs fast and smooth
\item[\itembulletMinus] Requires loading yet another package
\end{itemize}


\begin{table}%
\centering%
\begin{tabular}{l@{\hspace*{2em}}>{\biscolor{}\bisshape{circle}}c@{\hspace*{1em}}>{\biscolor{}\bisshape{square}}c@{\hspace*{1em}}>{\biscolor{blackwhite}\bisshape{circle}}c@{\hspace*{1em}}>{\biscolor{blackwhite}\bisshape{square}}c}
\toprule
\multicolumn{1}{r}{Color\hspace*{1em}} & \multicolumn{2}{c}{\texttt{color}} & \multicolumn{2}{c}{\texttt{blackwhite}}\\
\multicolumn{1}{r}{Shape\hspace*{1em}} & \texttt{circle} & \texttt{square} & \texttt{circle} & \texttt{square} \\
Command \\
\midrule
\lstinline|\itembulletEmpty| & \itembulletEmpty & \itembulletEmpty & \itembulletEmpty & \itembulletEmpty \\
\lstinline|\itembulletMinus| & \itembulletMinus & \itembulletMinus & \itembulletMinus & \itembulletMinus \\
\lstinline|\itembulletPlus| & \itembulletPlus & \itembulletPlus & \itembulletPlus & \itembulletPlus \\
\lstinline|\itembulletX| & \itembulletX & \itembulletX & \itembulletX & \itembulletX \\
\lstinline|\itembulletCheckmark| & \itembulletCheckmark & \itembulletCheckmark & \itembulletCheckmark & \itembulletCheckmark \\
\lstinline|\itembulletBlueBullseye| & \itembulletBlueBullseye & \itembulletBlueBullseye & \itembulletBlueBullseye & \itembulletBlueBullseye \\
\lstinline|\itembulletYellowBullseye| & \itembulletYellowBullseye & \itembulletYellowBullseye & \itembulletYellowBullseye & \itembulletYellowBullseye \\
\lstinline|\itembulletExclamationMarkRed| & \itembulletExclamationMarkRed & \itembulletExclamationMarkRed & \itembulletExclamationMarkRed & \itembulletExclamationMarkRed \\
\lstinline|\itembulletExclamationMarkYellow| & \itembulletExclamationMarkYellow & \itembulletExclamationMarkYellow & \itembulletExclamationMarkYellow & \itembulletExclamationMarkYellow \\
\lstinline|\itembulletInformation| & \itembulletInformation & \itembulletInformation & \itembulletInformation & \itembulletInformation \\
\lstinline|\itembulletQuestionMark| & \itembulletQuestionMark & \itembulletQuestionMark & \itembulletQuestionMark & \itembulletQuestionMark \\
\lstinline|\itembulletArrowRight| & \itembulletArrowRight & \itembulletArrowRight & \itembulletArrowRight & \itembulletArrowRight \\
\lstinline|\itembulletArrowLeft| & \itembulletArrowLeft & \itembulletArrowLeft & \itembulletArrowLeft & \itembulletArrowLeft \\
\lstinline|\itembulletArrowUp| & \itembulletArrowUp & \itembulletArrowUp & \itembulletArrowUp & \itembulletArrowUp \\
\lstinline|\itembulletArrowDown| & \itembulletArrowDown & \itembulletArrowDown & \itembulletArrowDown & \itembulletArrowDown \\
\lstinline|\itembulletArrowNorthWest| & \itembulletArrowNorthWest & \itembulletArrowNorthWest & \itembulletArrowNorthWest & \itembulletArrowNorthWest \\
\lstinline|\itembulletArrowNorthEast| & \itembulletArrowNorthEast & \itembulletArrowNorthEast & \itembulletArrowNorthEast & \itembulletArrowNorthEast \\
\lstinline|\itembulletArrowSouthWest| & \itembulletArrowSouthWest & \itembulletArrowSouthWest & \itembulletArrowSouthWest & \itembulletArrowSouthWest \\
\lstinline|\itembulletArrowSouthEast| & \itembulletArrowSouthEast & \itembulletArrowSouthEast & \itembulletArrowSouthEast & \itembulletArrowSouthEast \\
\bottomrule
\end{tabular}
\caption{Predefined symbols in different shapes and colors.
See Figure~\ref{fig:magnifiedpredefinedsymbols} for a selection of magnified symbols.
The usage of color pink for `arrow' symbols is subject to future changes.}
\label{tab:predefinedsymbols}
\end{table}

\begin{figure}
\centering%
{\Huge\biscolor{color}%
\itembulletEmpty%
\hspace{1ex}%
\itembulletBlueBullseye%
\hspace{1ex}%
\itembulletCheckmark%
\hspace{1ex}%
\itembulletExclamationMarkRed%
\hspace{1ex}%
\itembulletInformation%
\hspace{1ex}%
\itembulletQuestionMark%
\hspace{1ex}%
\itembulletPlus%
\hspace{1ex}%
\itembulletMinus%
\hspace{1ex}%
\itembulletArrowSouthEast%
\hspace{1ex}%
\itembulletX%
\hspace{1ex}%
\itembulletYellowBullseye%
}%
\caption{Selected predefined symbols, magnified.}
\label{fig:magnifiedpredefinedsymbols}
\end{figure}

\section{User-defined Symbols}
\label{sec:userdefinedsymbols}

User-defined symbols are described by four properties:
\begin{enumerate}
\item Their \emph{shape} that can be either \texttt{circle} or \texttt{square}.
\item Their (background) \emph{color} that can be
\begin{enumerate}
\item a predefine color gradient named \texttt{bisred}, \texttt{bisblue}, \texttt{bisgreen}, \texttt{bisyellow}, \texttt{bispink}, or \texttt{bisblack} -- or --
\item any color named in the \hreffootnote{http://ctan.org/pkg/xcolor}{\texttt{xcolor} package} (no gradient, solid color only).
\end{enumerate}
\biscolor{color}\itembulletInformation\ Those colors will get `converted' to black if the package is told to use the black/white mode either through a package option (see Section~\ref{sec:packageoptions}) or the \texttt{biscolor} command (see Section~\ref{sec:predefinedsymbolsconfiguration}).
\item Their \emph{symbol} that can be any of \texttt{empty}, \texttt{minus}, \texttt{plus}, \texttt{X}, \texttt{checkmark}, \texttt{bullseye}, \texttt{exclamationmark}, \texttt{information}, \texttt{questionmark}, \texttt{arrowright}, \texttt{arrowleft}, \texttt{arrowup}, \texttt{arrowdown}, \texttt{arrownorthwest}, \texttt{arrownortheast}, \texttt{arrowsouthwest}, or \texttt{arrowsoutheast}.
\item Their foreground color, which can be any color named in the \texttt{xcolor} package (note: \texttt{bisblue}, \texttt{bisgreen}, etc.\ are not available).
Similarly like the background color gets converted to black in black/white mode, the foreground color will be converted to white in this mode.
\end{enumerate}

The four properties can be passed to the command \lstinline|\useritembullet{shape}{bgcolor}{symbol}{fgcolor}| to create a new symbol.

It is recommended but not necessary to `wrap' \lstinline|\useritembullet| into a \lstinline|\newcommand| for easier access and consistency:
\begin{lstlisting}
\newcommand{\mybulletA}{\useritembullet{square}{bisyellow}{plus}{MediumVioletRed}}
\newcommand{\mybulletB}{\useritembullet{circle}{LightSlateGrey}{X}{pink}}
\begin{itemize}
\item[\mybulletA] First item
\item[\mybulletB] Second item
\item[\useritembullet{circle}{bisblue}{checkmark}{yellow}] Third item
\biscolor{blackwhite}
\item[\mybulletA] Fourth item (note the preceeding \verb|\biscolor{blackwhite}| command)
\end{itemize}
\end{lstlisting}
\biscolor{}% reset previous color settings
\newcommand{\mybulletA}{\useritembullet{square}{bisyellow}{plus}{MediumVioletRed}}
\newcommand{\mybulletB}{\useritembullet{circle}{LightSlateGrey}{X}{pink}}
\begin{itemize}
\item[\mybulletA] First item
\item[\mybulletB] Second item
\item[\useritembullet{circle}{bisblue}{checkmark}{yellow}] Third item
\biscolor{blackwhite}
\item[\mybulletA] Fourth item (note the preceeding \verb|\biscolor{blackwhite}| command)
\end{itemize}

\subsection{Customizations for Beamer}

Instead of keeping the colors of predefined item bullet symbols, those symbols can be redefined to match one's own color scheme.
For example, Beamer slides use the color \texttt{structure} for certain elements such as `normal' item bullets.
To match icons provided by this package to match the \texttt{structure}, redefine symbol commands like this:

\begin{lstlisting}
% Item bullet "information" should be in the slide's structure color
\renewcommand{\itembulletInformation}{\useritembullet{circle}{structure}{information}{white}}
\end{lstlisting}

\section{Package Options}
\label{sec:packageoptions}

Package options can be used to globally define the appearance of symbols.
The following options are available:

\begin{description}
\item[blackwhite] replaces all background colors by black and all foreground colors by white.
Useful for handouts or to save expensive color ink.
Corresponds to calling \lstinline|\biscolor{blackwhite}| (see Section~\ref{sec:predefinedsymbolsconfiguration}) at the document's beginning.
The same command can be used to change the color mode of all following symbol commands (including user-defined ones using \lstinline|\useritembullet|).
\item[color] actually uses the colors defined for a symbol as opposed to the option \texttt{blackwhite}.
Implicitly active by default.
Corresponds to calling \lstinline|\biscolor{color}| at the document's beginning.
This command affects the color mode of all following symbol commands (including those defined by \lstinline|\useritembullet|).
\item[square] draws all symbols in square shape.
Corresponds to calling \lstinline|\bisshape{square}| (see Section~\ref{sec:predefinedsymbolsconfiguration}) at the document's beginning.
The same command can be used to change the shape of pre-defined commands (but not those defined by \lstinline|\useritembullet|).
\item[circle] draws all symbols in circular shape.
Implicitly active by default.
Corresponds to calling \lstinline|\bisshape{circle}| at the document's beginning.
The same command can be used to change the shape of pre-defined commands (but not those defined by \lstinline|\useritembullet|).
\end{description}

An example on how to use package options:
\begin{lstlisting}
% In your preamble, load the package:
\usepackage[circle,blackwhite]{bulletitemsymbols}
\end{lstlisting}

\end{document}
