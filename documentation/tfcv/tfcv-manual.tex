%%
%
% tfcv is a LaTeX style for Curriculum Vitae documents.
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either
% version 1.3 of this license or (at your option) any
% later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status `maintained'.
%
% The Current Maintainer of this work is
%    Thomas Fischer <fischer@unix-ag.uni-kl.de>
%
% This work consists of the files tfcv.sty, tfcv-example.tex,
% and tfcv-manual.tex.
%
%%

\documentclass[a4paper,10pt]{article}
\usepackage{graphicx}
\usepackage[pdfborder={0 0 0}]{hyperref}
\usepackage{booktabs}
\usepackage{listings}
\usepackage{xcolor}
\let\mathcal\undefined% hack to make MnSymbol load
\usepackage{MnSymbol}
\usepackage{relsize}
\usepackage{array}
\usepackage{tikz}
\usepackage{tfcv}

\usetikzlibrary{patterns}

% unified source code listings
\lstset{escapechar=|,postbreak=\space, breakindent=5pt, captionpos=b, showtabs=false, tabsize=4, breaklines=true, floatplacement=t, basicstyle=\smaller\ttfamily, prebreak={\mbox{\ensuremath{\rhookswarrow}}}, backgroundcolor=\color{black!5}, keywordstyle=}
% leftmargin=2em, keywordstyle=\color{black!90}\textbf,
% numbers=left, numberstyle=\relsize{-2}, numberblanklines=false, numbersep=2ex,

\newcommand{\tfcv}{\texttt{tfcv}}

\title{\tfcv\ -- A \LaTeX{} Style for Curriculum Vitae documents\footnote{This manual is written for \tfcv\ designated as `2015/10/08 TF-CV'. Commands and options may differ in older or newer versions.}}
\author{Thomas Fischer\\\href{mailto:fischer@unix-ag.uni-kl.de}{fischer@unix-ag.uni-kl.de}\\\url{https://www.gitorious.org/latex-styles-and-classes/}}
\date{October 08, 2015}

\newenvironment{demonstration}{%
\smallskip\par\noindent%
\begin{minipage}{.99\linewidth}%
% \rule{\textwidth}{0.5pt}%
\begin{tikzpicture}%
\fill[pattern=north west lines,pattern color=black] (0,0) rectangle +(.99\linewidth,0.5ex);%
\draw (0,0) -- +(.99\linewidth,0);%
\end{tikzpicture}%
}{%
\noindent%
% \rule{\textwidth}{0.5pt}\newline%
\begin{tikzpicture}%
\draw (0,0) -- +(.99\linewidth,0);%
\fill[pattern=north west lines,pattern color=black] (0,0) rectangle +(.99\linewidth,-0.5ex);%
\end{tikzpicture}%
\end{minipage}%
\par%
}
\begin{document}

\maketitle

\begin{abstract}
\tfcv\ is a \LaTeX\ style for curriculum vitae (CV) documents.
The style contains all essential features to write tabular-styled curriculum vitae documents.
\end{abstract}

\section{Localization}

The text which is set by this class can be localized to a number of languages.
The desired language is set by passing an argument to the \texttt{usepackage} statement loading \texttt{tfcv}.
Supported languages include German (\texttt{german}), Swedish (\texttt{swedish}), and English (\texttt{english}, default if nothing else is specified).

\begin{lstlisting}
\usepackage[german]{tfcv}
\end{lstlisting}

Alternatively, the language option can be specified as a document class's option:

\begin{lstlisting}
\documentclass[german]{article}
\usepackage{tfcv}
\end{lstlisting}

A number of variables in form of commands are predefined.
Depending on the style's option, those variables get localized.
See Table~\ref{tab:predefinedvariables} for a list of available variables and selected translations.

\begin{table}
\centering
\begin{tabular}{p{.3\linewidth} p{.3\linewidth}p{.3\linewidth}@{}}
\toprule
Variable & English translation & German translation \\
\midrule
\verb|\cvname| & Name & Name \\
\verb|\cvacademicdegree| & Academic Degree & Akademischer Grad \\
\verb|\cvaddress| & Address & Adresse \\
\verb|\cvphone| & Phone & Telefon \\
\verb|\cvphonework| & Phone (work) & Telefon (Arbeit) \\
\verb|\cvphoneprivate| & Phone (home) & Telefon (privat) \\
\verb|\cvphonemobile| & Mobile & Mobiltelefon \\
\verb|\cvphonemobilework| & Mobile (work) & Mobiltelefon (Arbeit) \\
\verb|\cvphonemobileprivate| & Mobile (home) & Mobiltelefon (privat) \\
\verb|\cvweb| & WWW & WWW \\
\verb|\cvwww| & \multicolumn{2}{c}{\emph{same as \texttt{\char`\\{}cvweb}}} \\
\verb|\cvwebwork| & WWW (work) & WWW (Arbeit) \\
\verb|\cvwebprivate| & WWW (home) & WWW (privat) \\
\verb|\cvemail| & Email & E-Mail \\
\verb|\cvemailwork| & Email (work) & E-Mail (Arbeit) \\
\verb|\cvemailprivate| & Email (home) & E-Mail (privat) \\
\verb|\cvcitizenship| & Citizenship & Staatsb{\"u}rgerschaft \\
\bottomrule
\end{tabular}
\caption{List of predefined variables and selected translations.}
\label{tab:predefinedvariables}
\end{table}

To localize your own text, a number of commands are available which will evalute their content (e.\,g.\ print out the text) only if the document is set in a certain language.
If another language is used, the command's argument is ignored.

\begin{description}
\item[\texttt{\char`\\{}inEnglish}] sets a text fragment in English by calling Babel's \verb|\selectlanguage|. If the document's language is not English, the text will be set in italics as well.
\item[\texttt{\char`\\{}onlyWhenEnglish}] prints text only if the document is set in English.
\item[\texttt{\char`\\{}onlyWhenNotEnglish}] has the exact opposite behavior of \verb|\onlyWhenEnglish|.
\smallskip
\item[\texttt{\char`\\{}inGerman}] sets a text fragment in German by calling Babel's \verb|\selectlanguage|. If the document's language is not German, the text will be set in italics as well.
\item[\texttt{\char`\\{}onlyWhenGerman}] prints text only if the document is set in German; for any other language nothing will be printed.
\item[\texttt{\char`\\{}onlyWhenNotGerman}] has the exact opposite behavior of \verb|\onlyWhenGerman|.
\smallskip
\item[\texttt{\char`\\{}inSwedish}] sets a text fragment in Swedish by calling Babel's \verb|\selectlanguage|. If the document's language is not Swedish, the text will be set in italics as well.
\item[\texttt{\char`\\{}onlyWhenSwedish}] prints text only if the document is set in Swedish; for any other language nothing will be printed.
\item[\texttt{\char`\\{}onlyWhenNotSwedish}] has the exact opposite behavior of \verb|\onlyWhenSwedish|.
\end{description}

An example to describe a senior lecturer position at a Swedish university using the commands above looks like this:

\begin{lstlisting}
\onlyWhenEnglish{Senior Lecturer
    (`\inSwedish{universitetslektor}')}%
\onlyWhenGerman{Hochschullehrer
    (`\inSwedish{universitetslektor}')}%
\onlyWhenSwedish{Universitetslektor}
\end{lstlisting}

would result in the following output depending on the document's language:

{\centering
\begin{tabular}{ll}
\toprule
Document's language & Output \\
\midrule
English & Senior Lecturer (`\textit{universitetslektor}') \\
German & Hochschullehrer (`\textit{universitetslektor}') \\
Swedish & Universitetslektor \\
\bottomrule
\end{tabular}
\par}

\section{Text Formatting}

\subsection{Narrow Font}

A narrow variant of the Helvetica font is available on most TeX systems and accessible through two commands in \tfcv.

\begin{description}
\item[\texttt{\char`\\{}narrow}] typesets the text passed as the single parameter in a narrow font.
It is used in a similar manner like \verb|\emph{|\ldots\verb|}| or \verb|\textbf{|\ldots\verb|}|
\\Example: \verb|\narrow{Hello}| becomes \narrow{Hello}
%
\item[\texttt{\char`\\{}narrowfont}] typesets the following text in a narrow font until the end of the surrounding environment.
It is used in a similar manner like \verb|\sffamily| or \verb|\itshape|
\\Example: \verb|{ABC\narrowfont 123}| becomes {ABC\narrowfont 123}
\end{description}

\subsection{The CV Table}

Core concept of a tabular CV is obviously a table.
A preconfigured table is provided by a dedicated environment: \verb|cvtable|.
This environment can be configured globally, ensuring that all \verb|cvtable| environments look the same (for details see further below).

\begin{lstlisting}
\begin{cvtable}
Computer skills & \LaTeX \\
Driving license & Car and Truck
\end{cvtable}
\end{lstlisting}

becomes

\begin{demonstration}
\begin{cvtable}
Computer skills & \LaTeX \\
Driving license & Car and Truck
\end{cvtable}
\end{demonstration}

The table consumes the total line width.
The widths of the left column and the spacing between both columns can be configured through the lengths \verb|\cvleftcolumnwidth| and \verb|\cvspacingwidth|, respectively, the right column takes whatever space is left.

\begin{lstlisting}
\setlength{\cvleftcolumnwidth}{.6\linewidth}%
\setlength{\cvspacingwidth}{3em}%
\begin{cvtable}
Computer skills & \LaTeX \\
Driving license & Car and Truck
\end{cvtable}
\end{lstlisting}

becomes%

\begin{demonstration}
\setlength{\cvleftcolumnwidth}{.6\linewidth}%
\setlength{\cvspacingwidth}{3em}%
\begin{cvtable}
Computer skills & \LaTeX \\
Driving license & Car and Truck
\end{cvtable}
\end{demonstration}

Formatting of the two columns can be configured through \verb|\cvleftcolumn|\hspace{0pt}\verb|formatting| and \verb|\cvrightcolumnformatting|, which are commands that can be redefined.
Formattings like \verb|\narrowfont|, \verb|\raggedleft|, \verb|\bfseries|, or \verb|\sffamily| can be used.
In addition to the following example, see also Appendix~\ref{lbl:completeexample}.

\begin{lstlisting}
\renewcommand{\cvrightcolumnformatting}{\bfseries}%
\renewcommand{\cvleftcolumnformatting}{\narrowfont}%
\begin{cvtable}
Computer skills & \LaTeX \\
Driving license & Car and Truck
\end{cvtable}
\end{lstlisting}

becomes

\begin{demonstration}
\renewcommand{\cvleftcolumnformatting}{\narrowfont\raggedleft}%
\renewcommand{\cvrightcolumnformatting}{\bfseries}%
\begin{cvtable}
Computer skills & \LaTeX \\
Driving license & Car and Truck
\end{cvtable}
\end{demonstration}

\subsection{Time and Date}

To specify for how long you have worked at some place or the point in time where you achieved some goal, you have to specify the date.
Usually, at least the year has to be specified, but for more detailed information, month or season have to provided, too.

Several commands are availabe to formulate both single dates and time ranges.
Use those commands in the left column of \verb|cvtable|.

\begin{description}
\item[\texttt{\char`\\{}cvyear}] takes one mandatory parameter (the year) and optional parameter (either free text or a special value).
Special values for the optional parameter include numbers from \texttt{1} to \texttt{12} which get replaced by localized names of the twelve months and season identifiers \texttt{S1} to \texttt{S4} for `Spring' to `Winter'.
\\Example: \verb|\cvyear{2013}| stays \cvyear{2013}, \verb|\cvyear[5]{2013}| becomes \cvyear[5]{2013}, \verb|\cvyear[S2]{2013}| becomes \cvyear[S2]{2013}, and \verb|\cvyear[abc]{2013}| becomes \cvyear[abc]{2013}
%
\item[\texttt{\char`\\{}cvrange}] takes two parameters, such as two \verb|cvyear| commands.
\\Example: \verb|\cvrange{\cvyear[S4]{2011}}{\cvyear[6]{2012}}| becomes \cvrange{\cvyear[S4]{2011}}{\cvyear[6]{2012}}
%
\item[\texttt{\char`\\{}cvsince}] takes one parameter, such as normal text or a \verb|cvyear| command.
The text `since' is localized depending on the configured language.
\\Example: \verb|\cvsince{\cvyear[S3]{2012}}| becomes \cvsince{\cvyear[S3]{2012}}
\end{description}

\begin{appendix}

\section{A Complete Example}
\label{lbl:completeexample}

Here, a complete example including an image (see Fig.~\ref{fig:exampleoutput}) of the final output is provided.

\begin{figure}
\centering
\fbox{\includegraphics[width=.9\linewidth]{tfcv-example}}
\caption{Output of the example CV.}
\label{fig:exampleoutput}
\end{figure}

\lstinputlisting{tfcv-example.tex}

\end{appendix}

\end{document}
