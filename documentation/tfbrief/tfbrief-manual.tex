%%
%
% tfbrief is a LaTeX class for letters written in German, English,
% Swedish, or Spanish
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status `maintained'.
%
% The Current Maintainer of this work is
%    Thomas Fischer <fischer@unix-ag.uni-kl.de>
%
% This work consists of the files tfbrief.cls, tfbrief-beispiel.tex
% and tfbrief-manual.tex.
%
%%

\documentclass[a4paper,10pt]{article}
\usepackage[english]{babel}
\usepackage{graphicx}
\usepackage[pdfborder={0 0 0}]{hyperref}
\usepackage{booktabs}

\newcommand{\dc}[1]{\texttt{#1}}
\newcommand{\tfbrief}{\dc{tfbrief}}
\newcommand{\optfield}{\ensuremath{^\circ}}

\title{\tfbrief{} -- A \LaTeX{} Document Class for Letters\footnote{This manual is written for \tfbrief{} designated as `2012/08/22 TF-Brief'. Commands and options may differ in older or newer versions.}}
\author{Thomas Fischer\\\href{mailto:fischer@unix-ag.uni-kl.de}{fischer@unix-ag.uni-kl.de}\\\url{https://www.gitorious.org/latex-styles-and-classes/}}
\date{August 22, 2012}

\begin{document}

\maketitle

\begin{abstract}
\tfbrief{} is a document class for letters.
It was originally designed for letters in the German \emph{sprachraum}, which are printed on A4 paper (DIN EN ISO 216).
The document class contains all essential features to write letters in a private-to-business or business-to-business correspondence.
\end{abstract}

\section{Introduction}

In Germany, the design of business letters is specified by two DIN standards.
DIN~676 regulates the overall structure of the letter including header and footer.
DIN~5008 defines the `inner' structure of the letter regarding the format and layout of the text body.
\tfbrief{} as documented in this manual does not follow the above standards exactly, but inherits most of their features.

As a \LaTeX{} document class, \tfbrief{} follows the separation of layout and content.
Writing letters in \tfbrief{} thus consists of two phases.
First, the user specifies all meta information (most importantly, sender and receiver addresses) in the \LaTeX{} document's header (before \verb|\begin{document}|).
Second, in the document itself, only a single environment (\verb|letter|) is used for the text body.
Optionally, a list of attachments to the letter can be specified.

Figure~\ref{fig:exampleoutput} (p.\,\pageref{fig:exampleoutput}) shows an example how letters look like when set with \LaTeX{} and \tfbrief{}.

\section{Specifying Meta Information}

In the following list, all possible meta information fields are summarized.
These commands are indented to be written before the document's \verb|\begin{document}| command.

Some fields are mandatory (e.\,g.~the receiver's address), other fields are optional and will be marked by the symbol `\optfield'.
If no text for an optional field is specified, \tfbrief{} may insert a default value instead.

\begin{description}
\item[fromname] Name of the sender.
An optional parameter may specify a short version of the name.\\
Example: \verb|\fromname[E. Goldstein]{Emanuel Goldstein}|
\item[fromtitle] Title (e.\,g.\ function in an organization) of the sender.
An optional parameter may specify a variant to be used in the signature at the letter's end instead of the title of the mandatory argument.\\
Example: \verb|\fromtitle{Vizedirektor f{\"u}r Forschung}|
\item[fromdegreepre\optfield] Academic degree that is written before the name.
To have a different academic degree at your name at the letter's bottom opposed to the degree used in the sender field at the first page's top, use an optional parameter.
\\Example: \verb|\fromdegreepre[Dr.]{Dr.~med.}|
\item[fromdegreepost\optfield] Academic degree that is written after the name. Between the name and the degree a colon will be put automatically.
To have a different academic degree at your name at the letter's bottom opposed to the degree used in the sender field at the first page's top, use an optional parameter.
\\Example: \verb|\fromdegreepost{M.\,D.}|
\item[fromstreet] Street of the sender, including number. Again, an optional parameter may specify a short version.\\Example: \verb|\fromstreet[Platz. d. dt. Einheit 5a]{Platz der|\\
\verb|deutschen Einheit 5a\\Raum A.502}|
\item[fromcity] City of the sender.\\Example: \verb|\fromcity[B-Neuk{\"o}lln]{Berlin-Neuk{\"o}lln}|
\item[fromcitycode\optfield] Postal code of the sender's location. Optional for countries without postal codes (e.\,g.~Republic of Ireland).\\Example: \verb|\fromcitycode{10352}|
\item[fromcitycc\optfield] Country code of the sender's location. May be deprecated in some countries.\\Example: \verb|\fromcitycc{D}|
\item[fromcountry\optfield] Name of the country the sender is located in.
An optional parameter may specify a special version used in the fine print above the receipient's address, e.\,g.~to specify the English name variant for better handling in international shippings.
\\Example: \verb|\fromcountry[Norway]{Norge}|
\item[fromphone\optfield] The sender's phone number.
May have an optional parameter to change the prepended label.
\\Example: \verb|\fromphone{0\,30\,/\,5\,14\,35-156}|
\item[fromfax\optfield] The sender's fax number.
May have an optional parameter to change the prepended label.
\\Example: \verb|\fromfax{0\,30\,/\,5\,14\,35-151}|
\item[frommobile\optfield] The sender's mobile number.
May have an optional parameter to change the prepended label.
\\Example: \verb|\frommobile{01\,72\,/\,78\,65\,34\,12}|
\item[fromemail\optfield] The sender's email address.
May have an optional parameter to change the prepended label.
\\Example: \verb|\fromemail{emanuel.goldstein@example.com}|
\item[fromwww\optfield] The sender's WWW address.
May have an optional parameter to change the prepended label.
\\Example: \verb|\fromwww{http://www.example.com}|
\item[ourref\optfield] A number the sender assigns to this letter allowing the receiver to refer to this letter in a later answer.
May have an optional parameter to change the prepended label.
\\Example: \verb|\ourref[serial number]{S/N 509307-A0}|

\medskip

\item[toname] Name of the receiver.\\Example: \verb|\toname{Bundesamt f{\"u}r|\\
\verb| Fr{\"u}hst{\"u}cksflocken\\z.\,H. Frau Mahlzahn}|
\item[tostreet] Street of the receiver, including number.\\Example: \verb|\tostreet{An der Spree 3}|
\item[tocity] City of the receiver.\\Example: \verb|\tocity{Berlin}|
\item[tocitycode\optfield] Postal code of the receiver's location. Optional for countries without postal codes (e.\,g.~Republic of Ireland).\\Example: \verb|\tocitycode{10176}|
\item[tocitycc\optfield] Country code of the receiver's location.
May be deprecated in some countries.
Use either this field or \verb|\tocountry|.
\\Example: \verb|\tocitycc{D}|
\item[tocountry\optfield] Name of the country the receiver is located in.
Use either this field or \verb|\tocitycc|.
\\Example: \verb|\tocountry{Germany}|
\item[tofax\optfield] The receiver's fax number.
Will be printed in a box below the postal address.
For best visual results, do not use this field in conjunction with \verb|\toemail|.
\\Example: \verb|\tofax{0\,30\,/\,4\,22\,05-114}|
\item[toemail\optfield] The receiver's email address.
Will be printed in a box below the postal address.
For best visual results, do not use this field in conjunction with \verb|\tofax|.
\\Example: \verb|\toemail{sales@aol.com}|
\item[yourref\optfield] A number the receiver assigned to a letter previously sent to this letter's sender.
May have an optional parameter to change the prepended label.
\\Example: \verb|\yourref[customer number]{ATV-2008352}|

\medskip

\item[subject] Short description of the letter's content.\\Example: \verb|\subject{Untersuchung von|\\
\verb|Fr{\"u}hst{\"u}cksflocken\\|\\
\verb|Aktenzeichen: 58636-AZG -- HA\,3\\Bearbeiter: Frau Mahlzahn}|
\item[date] Date when the letter was officially written.\\Example: \verb|\date{15. Februar 2007}|
\item[greeting\optfield] Greeting at the beginning of the letter.\\Example: \verb|\greeting{Sehr geehrte Frau Mahlzahn,}|
\item[leave\optfield] Farewell at the end of the letter.\\Example: \verb|\leave{Hochachtungsvoll,}|
\item[signature\optfield] Place a scan of your signature here if you do not sign the letter by hand (e.\,g.~if you send it by email).
\\Example: \verb|\signature{\includegraphics[height=3em]{mysig}}|
\item[logo\optfield] Corporate identity logo.
Place either text or a graphics here.
\\Example: \verb|\logo{\Huge\LaTeX}|
\item[footer\optfield] Text block at the bottom of the first page, printed in a small, narrow font.
Used e.\,g.~for account information or other details.
Command may have an optional parameter to specify the footer's height; the default value is \verb|1cm|.
\\Example:
\begin{verbatim}
\footer{
\protect\begin{tabular}{@{}ll@{\hspace*{1cm}}ll}
Kontonummer & 123 456 789 &
     Gesch{\"a}ftsf{\"u}hrer & Herr Dr. Kl{\"o}bner \\
Bankleitzahl & 987 654 321 00 &
     Gerichtsstand & Cayman-Inseln
\protect\end{tabular}
}
\end{verbatim}
In this example, \verb|\protect| is used to protect the \texttt{tabular} environment commands.
In the table's column parameter, \verb|@{}| removes the table's left margin, so that the table's content starts aligned to the letter's text.
\end{description}

In some cases (e.\,g.~when using multiline street names), computing the width of the box containing the sender's meta data may fail resulting in a corrupted page layout.
You may provide a hint on the box's width using the \verb|\fromwidth| command.
In the following example, simply insert a text string which is used to evaluate the correct width:
\begin{verbatim}
\settowidth{\fromwidth}{Platz der deutschen Einheit 5a}
\end{verbatim}


In the following \LaTeX{} source code, a complete example of a header is given.

\begin{verbatim}
\settowidth{\fromwidth}{Platz der deutschen Einheit 5a}

\fromname{Emanuel Goldstein}
\fromdegreepre[Dr.]{Dr. med.}
\fromdegreepost[M.\,D.]{}
\fromstreet[Platz. d. dt. Einheit 5a]{Platz der deutschen
            Einheit 5a\\Raum A.502}
\fromcity[B-Neuk{\"o}lln]{Berlin-Neuk{\"o}lln}
\fromcitycode{10352}
\fromcitycc{D}
\fromcountry{Germany}
\fromphone{0\,30\,/\,5\,14\,35-156}
\fromfax{0\,30\,/\,5\,14\,35-151}
\frommobile{01\,72\,/\,78\,65\,34\,12}
\fromemail{emanuel.goldstein@example.com}
\fromwww{http://www.example.com}
\ourref{ATV-2008352}

\toname{Bundesamt f{\"u}r Fr{\"u}hst{\"u}cksflocken\\
        z.\,H. Frau Mahlzahn}
\tostreet{An der Spree 3}
\tocity{Berlin}
\tocitycode{10176}
\tocitycc{D}
\tocountry{Germany}
\tofax{0\,30\,/\,4\,22\,05-114}
\yourref{58636-AZG -- HA\,3}

\date{15. Februar 2007}
\subject{Untersuchung von Fr{\"u}hst{\"u}cksflocken\\
         Aktenzeichen: 58636-AZG -- HA\,3\\
         Bearbeiter: Frau Mahlzahn}
\greeting{Sehr geehrte Frau Mahlzahn,}
\leave{Hochachtungsvoll,}
\logo{\Huge\LaTeX}
\end{verbatim}



\section{A Letter's Text Body}

Once a valid header including all required meta data is specified, the main text body may be written.
As mentioned above, \LaTeX{} and \tfbrief{} take care of layouting the letter, so only a single environment (\verb|letter|) within the document's body is sufficient as shown in the following example:

\begin{verbatim}
\begin{document}
\begin{letter}
anbei erhalten Sie wie im Schreiben vom 21.02.2007 angefragt
die bisherigen Unter\-suchungs\-ergebnisse f{\"u}r die
Fr{\"u}hst{\"u}cksflocken der Marke \glqq{}Roter Morgen\grqq,
hergestellt von den \glqq{}Nahrungs\-mittel\-werke Ernst
Mosch\grqq{} in Castrup-Brauxel.

Nach den bisherigen Ergebnissen k{\"o}nnen die
Fr{\"u}hst{\"u}cksflocken nicht f{\"u}r den Verzehr empfohlen
werden. Die untersuchten Proben weisen einen Knusprigkeitsgrad
von 1,7\,k auf und liegen damit unter dem gesetzlichen
Grenzwert von 3,2\,k.

Ich hoffe, diese Informationen sind Ihnen f{\"u}r Ihre Arbeit
behilflich.
\end{letter}
\end{document}
\end{verbatim}

If a list of documents is attached to a print-out of this letter, this list can be specified in a special list environment.
This list environment has to be placed between the end of the \verb|letter| environment and the end of the \verb|document| environment.

\begin{verbatim}
[...]
\end{letter}

\begin{attachment}
\item Vorl{\"a}ufige Untersuchungsergebnisse
      (mit Durchschlag auf blauem Papier)
\end{attachment}

\end{document}
\end{verbatim}

The attachment environment may have an optional parameter to replace the standard label (`attachment' in English, `Anlagen' in German).

\begin{verbatim}
[...]
\begin{attachment}[For further information see]
[...]
\end{verbatim}

\section{Class Options}

Document class \tfbrief{} is derived from \dc{scrartcl} and thus supports all options from this class.
By default, options \texttt{a4paper} and \texttt{12pt} are passed to \dc{scrartcl}.

\tfbrief{} itself offers the following options:

\begin{description}
\item[english] The option \texttt{english} changes all German text strings (which are used by default) to their English counterparts.
The following non-complete list gives some examples:

{
\centering
\begin{tabular}{ll}
\toprule
German & English \\
\midrule
Telefon & phone \\
per FAX an & sent via FAX to \\
Mit freundlichen Gr{\"u}{\ss}en & Sincerely \\
\bottomrule
\end{tabular}
}

\item[spanish] This option uses Spanish text strings instead of their German counterparts.
This translation was provided by Martin van Driel\footnote{\url{martin@vandriel.de}}.

\item[swedish] This option uses Swedish text strings instead of their German counterparts.

\item[nofoldingmark] Do not print folding marks on the first page's left side.

\item[fromheadernarrow] Using option \texttt{fromheadernarrow}, the letter's from header (your own address and contact data) will be set more narrow, but consumes more vertical space.
In the following example, the left side represents the standard variant (without this option), the the right side shows the case where the option is set.

\fbox{\begin{minipage}[t][4.5em]{.45\linewidth}%
\makebox[1.5cm][r]{\itshape\footnotesize{}E-Mail\hspace{1em}}info@example.com\\%
\makebox[1.5cm][r]{\itshape\footnotesize{}Telefon\hspace{1em}}01\,72\,/\,78\,65\,34\,12\\%
\end{minipage}}%
\hfill%
\fbox{\begin{minipage}[t][4.5em]{.45\linewidth}%
\hspace*{1.5cm}{\itshape\footnotesize\vspace*{-0.25em}E-Mail}\\\hspace*{1.5cm}info@example.com\\%
\hspace*{1.5cm}{\itshape\footnotesize\vspace*{-0.25em}Telefon}\\\hspace*{1.5cm}01\,72\,/\,78\,65\,34\,12%
\end{minipage}}
\end{description}


\section{Dependencies}

\tfbrief{} relies the following packages to build proper documents:
\dc{vmargin}, \dc{ifthen}, \dc{textcase}, \dc{microtype}, \dc{fixltx2e}, \dc{mparhack}, \dc{ragged2e}.\\
These packages are usually installed on a full TDS-compliant system.
All packages except for the first three may be removed from \tfbrief{}'s source code if not available on your system.

\section{A Complete Example}

Here, a complete example including an image (see Fig.~\ref{fig:exampleoutput}) of the final output is provided.

\begin{figure}
\centering
\fbox{\includegraphics[width=.9\linewidth]{tfbrief-beispiel}}
\caption{Output of the example letter.}
\label{fig:exampleoutput}
\end{figure}


\begin{verbatim}
\documentclass{tfbrief}
\usepackage[german]{babel}

% set the width of the "from" column by using the width of
% a given text should only be neccessary, if it cannot be
% automatically detected (e.g. with multiline fromstreet)
\settowidth{\fromwidth}{Platz der deutschen Einheit 5a}

\fromname{Emanuel Goldstein}
\fromdegreepre[Dr.]{Dr. med.}
\fromdegreepost[M.\,D.]{}
\fromstreet[Platz. d. dt. Einheit 5a]{Platz der deutschen
            Einheit 5a\\Raum A.502}
\fromcity[B-Neuk{\"o}lln]{Berlin-Neuk{\"o}lln}
\fromcitycode{10352}
\fromcitycc{D}
\fromcountry{Germany}
\fromphone{0\,30\,/\,5\,14\,35-156}
\fromfax{0\,30\,/\,5\,14\,35-151}
\frommobile{01\,72\,/\,78\,65\,34\,12}
\fromemail{emanuel.goldstein@example.com}
\fromwww{http://www.example.com}
\ourref{ATV-2008352}

\toname{Bundesamt f{\"u}r Fr{\"u}hst{\"u}cksflocken\\
        z.\,H. Frau Mahlzahn}
\tostreet{An der Spree 3}
\tocity{Berlin}
\tocitycode{10176}
\tocitycc{D}
\tocountry{Germany}
\tofax{0\,30\,/\,4\,22\,05-114}
\yourref{58636-AZG -- HA\,3}

\date{15. Februar 2007}
\subject{Untersuchung von Fr{\"u}hst{\"u}cksflocken\\
         Aktenzeichen: 58636-AZG -- HA\,3\\
         Bearbeiter: Frau Mahlzahn}
\greeting{Sehr geehrte Frau Mahlzahn,}
\leave{Hochachtungsvoll,}
\logo{\Huge\LaTeX}

\begin{document}
\begin{letter}
anbei erhalten Sie wie im Schreiben vom 21.02.2007 angefragt
die bisherigen Unter\-suchungs\-ergebnisse f{\"u}r die
Fr{\"u}hst{\"u}cksflocken der Marke \glqq{}Roter Morgen\grqq,
hergestellt von den \glqq{}Nahrungs\-mittel\-werke Ernst
Mosch\grqq{} in Castrup-Brauxel.

Nach den bisherigen Ergebnissen k{\"o}nnen die
Fr{\"u}hst{\"u}cksflocken nicht f{\"u}r den Verzehr empfohlen
werden. Die untersuchten Proben weisen einen Knusprigkeitsgrad
von 1,7\,k auf und liegen damit unter dem gesetzlichen Grenzwert
von 3,2\,k.

Ich hoffe, diese Informationen sind Ihnen f{\"u}r Ihre Arbeit
behilflich.

\end{letter}

% if you like to have the attachment closer to the page bottom,
% enlarge the current page
\enlargethispage{1cm}

\begin{attachment}
\item Vorl{\"a}ufige Untersuchungsergebnisse (mit Durchschlag
      auf blauem Papier)
\end{attachment}

\end{document}
\end{verbatim}

\end{document}
