%%
%
% linkimage is a LaTeX package to link small images within a Beamer
% presentation to a full-screen variant of the same image at the
% slide set's end.
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status `maintained'.
%
% The Current Maintainer of this work is
%    Thomas Fischer <fischer@unix-ag.uni-kl.de>
%
% This work consists of the files linkimage.sy, linkimage-example.tex
% and linkimage-manual.tex.
%
%%

\documentclass[a4paper,10pt]{article}
\usepackage[english]{babel}
\usepackage{graphicx}
\usepackage[pdfborder={0 0 0}]{hyperref}
\usepackage{booktabs}
\usepackage{listings}
\usepackage{relsize}
\usepackage{MnSymbol}
\usepackage{xcolor}

% unified source code listings
\lstset{postbreak=\space, breakindent=5pt,language=[LaTeX]TeX,
    captionpos=b, showtabs=false, tabsize=4, breaklines=true, floatplacement=t,
    basicstyle=\ttfamily, keywordstyle=\color{green!50!black}\textbf,
    numbers=left, numberstyle=\relsize{-2}, numberblanklines=false, numbersep=2ex,
    morekeywords={subtitle,subject,frametitle,includegraphics,beamerbutton,titlepage,linkimage,flushlinkimages,printindex},
    xleftmargin=2em, prebreak={\mbox{\ensuremath{\rhookswarrow}}}}

\newcommand{\dc}[1]{\texttt{#1}}
\newcommand{\linkimage}{\dc{linkimage}}
\newcommand{\optfield}{\ensuremath{^\circ}}

\title{\linkimage{} -- A \LaTeX{} Package to Link Images Across a Slide Set\footnote{This manual is written for \linkimage{} designated as `2012/07/29 LinkImage'. Commands and options may differ in older or newer versions.}}
\author{Thomas Fischer\\\url{fischer@unix-ag.uni-kl.de}\\\url{https://www.gitorious.org/latex-styles-and-classes/}}
\date{July 29, 2012}

\begin{document}

\maketitle

\begin{abstract}
\linkimage{} package to link small images within a Beamer presentation to a full-screen variant of the same image at the slide set's end.
\end{abstract}

\section{Introduction}

The motivation for this package is the following situation:
within a presentation, you have only space for a small, scaled variant of an image or photo you want to show.
However, you audience should be able to view the same image in a larger scale as well.

A solution to this problem is provided by this package.
If used properly, you simply click on the small, scaled variant of image and you presentation jumps to the large version which is on its own slide at the end of the slide set.
Once done with viewing the large version, click on the large version and your presentation jumps back where you came from.

\section{Commands}

Only two commands are necessary to operate this package.

\begin{description}
\item[linkimage] Specify item to click on to jump to large image.
This command has two mandatory parameters and one optional.
The first mandatory parameter is the item to show and where you would click on.
Usually, this would be an \lstinline|\includegraphics| command, but it could be as well just plain text or a \lstinline|\beamerbutton| command.
The second mandatory parameter is just the image's file name (not an \lstinline|\includegraphics| command).
The optional parameter is the title to show on the slide of the large-scale image variant.

Examples:
\begin{lstlisting}
\linkimage{\includegraphics[height=4em]{laufen-hut}}{laufen-hut}
\linkimage[Full-Size Version of Aperture]{\includegraphics[height=4em]{laufen-hut-aperture}}{laufen-hut}
\linkimage{\beamerbutton{Show Photo}}{laufen-hut}
\end{lstlisting}

\item[flushlinkimages]
This command tells the package to insert the extra slides for the large image variants right where the command is located.
Usually, you would place this command right before \lstinline|\end{document}|, similar to \lstinline|\printindex| of MakeIndex.
\end{description}

\section{Dependencies}

\linkimage{} relies the following packages to build proper documents:
\dc{graphics}, \dc{hyperref}, \dc{ifthen}.\\
These packages are usually installed on a full TDS-compliant system.

\section{A Complete Example}

Here, a complete example is provided.

\begin{lstlisting}
\documentclass[t,english]{beamer}
\usepackage{babel}
\usepackage{linkimage}

\subject{Write subject here}
\title{Some Longer Title containing a Line Break}
\subtitle{Write subtitle here}
\author{Hein Bl{\"o}d}
\date{Nov 11, 2009}

\begin{document}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}
\frametitle{Frame with Small Image}

\begin{itemize}
\item Click on one of the small photos to jump to its large version at end of slide set
\item Click then on large variant to jump back here
\medskip
\item Photo taken from Wikimedia Commons, was \emph{Photo of the Day} on July 10, 2012
\item Photo was released under the Creative Commons Attribution 3.0 Unported License by user Morray
\end{itemize}
\medskip
\centering
\linkimage{\includegraphics[height=4em]{laufen-hut}}{laufen-hut}%
\hspace{3em}%
\linkimage[Full-Size Version of Aperture]{\includegraphics[height=4em]{laufen-hut-aperture}}{laufen-hut}

\end{frame}

\begin{frame}
\frametitle{Some more Frames}

\bigskip
\centering
Nothing to see here

\end{frame}

\begin{frame}
\frametitle{The End}

\bigskip
\centering
Thank you for your attention!

\medskip
(you would stop your presentation here)

\end{frame}

\flushlinkimages

\end{document}
\end{lstlisting}

\end{document}
